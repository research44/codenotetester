/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.messaging.codenotetester;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class TestCodeCoach {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        ExecutorService es = Executors.newFixedThreadPool(500);
        for (int i = 0; i < 500; i++) {
            final int taskIndex=i;
            es.submit(() -> {
                try {
                    long start=System.currentTimeMillis();
                    Logger.getLogger(TestCodeCoach.class.getName()).info("Task "+taskIndex+" submitted");
                    CookieManager cookieManager = new CookieManager();
                    CookieHandler.setDefault(cookieManager);
                    HttpRequest request = HttpRequest.post("http://localhost:1025/createproject").form(Map.of("stuid", "11111111", "progectname", "Phaser Game"));
                    int code = request.code();
                    String str = request.body();
                    //extract projectId
                    int index=str.indexOf("var projectid = \"");
                    int index2=str.indexOf("\";", index+1);
                    String projectId=str.substring(index+"var projectid = \"".length(), index2);
                    System.out.println(projectId);
                    str=HttpRequest.get("http://localhost:1025/monacoLoader?projectId="+projectId).body();
                    //extract project structure from returned web page
                    index = str.indexOf("var project = ");
                    index2 = str.indexOf("var stuid =", index + 1);
                    String json = str.substring(index + "var project = ".length(), index2);
                    json = json.substring(0, json.lastIndexOf(";"));
                    System.out.println(json);
                    Gson gson = new Gson();
                    Map projectStructure = gson.fromJson(json, Map.class);
                    //////////////////////////////////////////////////
//                    Thread.sleep(1000);
                    String url = "http://localhost:1025/ai/codeCoach";
                    String payload=
                            """
                                            {
                                              "projectId": "${projectId}",
                                              "fileFilter": [],
                                              "errors": [
                                                {
                                                  "message": "missing ) after argument list",
                                                  "fileName": "http://localhost:1025/preview/d09e436c-f375-488f-bc98-b6dd4c965d42/Scene_answer.js?A1709195035434",
                                                  "lineNumber": 22,
                                                  "type": "SyntaxError"
                                                },
                                                {
                                                  "message": "Scene is not defined",
                                                  "fileName": "http://localhost:1025/preview/d09e436c-f375-488f-bc98-b6dd4c965d42/index.html?preview=true&projectId=01386f1f-23f2-4541-84d9-8130792cfce7",
                                                  "lineNumber": 29,
                                                  "type": "ReferenceError"
                                                }
                                              ]
                                            }
                                            """.replace("${projectId}", projectId);
                    request = HttpRequest.post(url).contentType("application/json;charset=utf-8").send(payload);
                    int ret = request.code();
                    String ticketId=request.body();
                    System.out.println(""+taskIndex+" ticket="+ticketId);
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(TestCodeCoach.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    request=HttpRequest.get("http://localhost:1025/ai/codeCoach/result/"+ticketId).contentType("appplication/json;charset=utf-8");
                    ret=request.code();
                    String response=request.body();
                    long ok=System.currentTimeMillis();
                    Logger.getLogger(TestCodeCoach.class.getName()).info("Task "+taskIndex+" finished in "+(ok-start)+" milliseconds with ret="+ret+", response="+response);
                } catch (Exception ex) {
                    Logger.getLogger(TestCodeCoach.class.getName()).log(Level.SEVERE, null, ex);
                }
            });

        }
        es.shutdown();
        Logger.getLogger(TestCodeCoach.class.getName()).info("awaiting all tasks termination");
        es.awaitTermination(1, TimeUnit.DAYS);
        Logger.getLogger(TestCodeCoach.class.getName()).info("all tasks finished");
        System.exit(0);
    }
    
}
