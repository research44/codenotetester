/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.messaging.codenotetester;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
public class TestSaveCode {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        ExecutorService es = Executors.newFixedThreadPool(1000);
        for (int i = 0; i < 1000; i++) {
            final int taskIndex=i;
            es.submit(() -> {
                long start=System.currentTimeMillis();
                Logger.getLogger(TestSaveCode.class.getName()).info("Task "+taskIndex+" submitted");
                CookieManager cookieManager = new CookieManager();
                CookieHandler.setDefault(cookieManager);
                HttpRequest request = HttpRequest.post("http://localhost:1025/createproject").form(Map.of("stuid", "11111111", "progectname", "Phaser Game"));
                int code = request.code();
                String str = request.body();
                
                //extract project structure from returned web page
                int index = str.indexOf("var project = ");
                int index2 = str.indexOf("var stuid =", index + 1);
                String json = str.substring(index + "var project = ".length(), index2);
                json = json.substring(0, json.lastIndexOf(";"));
                Gson gson = new Gson();
                Map projectStructure = gson.fromJson(json, Map.class);
                //////////////////////////////////////////////////
                String projectId = (String) projectStructure.get("projectid");
                List codeBeans = (List) projectStructure.get("codeinfoes");
                Map codeBean = (Map) codeBeans.get(0);
                Map payload = Map.of(
                        "diffArray", List.of(),
                        "cib", codeBean
                );
                String url = "http://localhost:1025/savecode?projectId=" + projectId;
                request = HttpRequest.post(url).contentType("application/json;charset=utf-8").send(gson.toJson(payload));
                int ret = request.code();
//                System.out.println(ret);
                long ok=System.currentTimeMillis();
                Logger.getLogger(TestSaveCode.class.getName()).info("Task "+taskIndex+" finished in "+(ok-start)+" milliseconds with ret="+ret);
            });

        }
        es.shutdown();
        Logger.getLogger(TestSaveCode.class.getName()).info("awaiting all tasks termination");
        es.awaitTermination(1, TimeUnit.DAYS);
        Logger.getLogger(TestSaveCode.class.getName()).info("all tasks finished");
        System.exit(0);
    }

}
